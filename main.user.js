// ==UserScript==
// @name         МЯТеж
// @namespace    FM veshtschaniye
// @version      0.0.0.1
// @author       AHOHNMYC
// @match        https://anon.fm/info.html
// @grant        unsafeWindow
// ==/UserScript==

function strHash(str) {
    let hash = 0;
    if (str.length == 0)
        return hash;
    for (let i = 0; i < str.length; i++) {
        let char = str.charCodeAt(i);
        hash = ((hash<<5)-hash)+char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

/*
function hashToName(hash) {
    let s = '';
    for (let i=0; i<5; i++) {
        s += String.fromCharCode('A'.charCodeAt() + (hash & 0b1111) + (((hash >> 4) & 0b1) == 1 ? 10 : 0));
        hash >>= 5;
    }
    s += String.fromCharCode('A'.charCodeAt() + 6 * (hash & 0b1) + 4 * (hash & 0b10));
    return hash;
}
*/

function hashToMyata(hash) {

    if (!hash)
        return "";

    const o = JSON.parse('{"Г":["Л","В","А","О"],"Л":["А","Ю","У","О"],"А":["М","В","Н","Р","Т","Ч","З","Д"],"В":["А","О","И"],"С":["Е","Т","А","Л","И","У"],"Е":["М","Р","П","Д","В","З","Ж","С","Н"],"Р":["Ж","Д","Е","И","А","О"],"Ж":["А","Н","Е"],"Н":["Т","О","А","Е","Ы","И","Н"],"Д":["И","А","К"],"И":["М","И","Н","С","Р","Л","Е"],"О":["Ж","С","Е","К","В","Р","Д","О","Т","П"],"Т":["Ы","У","Е","А"],"К":["Р","А"],"П":["О","У","Р"],"Ю":["О"],"У":["Г","П","К","Д","Е"],"Ч":["Е"],"З":["В","Л"],"М":["Я"],"Я":["Т"]}');
    const p = 'ГЛАВСЕРЖНДИОТКПЮУЧЗМЯ';

    let lettr = p[(hash & 0b1111) + (((hash >> 4) & 0b1) == 1 ? 5 : 0)];
    hash >>= 5;
    let out = lettr;
    let leftBits = 32 - 5;

//    debugger

    while (leftBits > 0) {
        const nextPossiblLen = o[lettr].length;
        let t = 0;
        if (nextPossiblLen > 1) {
            t = (hash & 0b111);
            hash >>= 3;
            leftBits -= 3;
            while (t >= nextPossiblLen)
                t -= Math.trunc(nextPossiblLen/2) + 1;
        }
        lettr = o[lettr][t];
        out += lettr.toLowerCase();
    }
    return out;
}

const _renderKooka = renderKooka;
unsafeWindow.renderKooka = (type, hash, quote, time, who, text) => {
    const respec = text.match(/<br><br>С уважением, (\W+ \W+ \W+)/);

    if (!respec)
        return _renderKooka(type, hash, quote, time, who, text);

    const name = respec[1];
    text = text.replace(respec[0], '');

    if (hash == '!')
        return `<p><span class="user_id">${hashToMyata(strHash(name))}</span> в ${time} - ${text}</p>`;
    else
        return `<p><span class="user_id">${hashToMyata(strHash(hash))}</span> в ${time} - <span class="userpost">${quote}</span><br><span class="user_id">${hashToMyata(strHash(name))}: </span>${text}</p>`;
}
